var ndobject = require('ndobject');

module.exports = function() {
    var {_private, _public, _parent} = new ndobject().extend();
    
    _public.initialize = function(room) {
        _private.room = room;
    }
    _public.getRoom = function() { return _private.room; }
    
    // The default state object to be cloned and passed into run()
    _public.getDefaultState = function() { return {}; }
    // Returns a basic slug string naming the role we manage.
    _public.getRole = function() { return undefined; }
    // Return the targetted number of creeps of this role.
    // Return an array of [minimum, target, maximum]. How exactly this
    // is implemented is yet to be determined.
    _public.getTarget = function() { return [0, 0, 0]; }
    // Return the creep specifications we want for this role. This is
    // an array of specifications, with the most desirable first and the
    // less desirable later. We will wait and spawn the best _possible_.
    // If the spawn isn't upgraded enough to spawn the higher levels, we'll
    // spawn a lower level for now.
    // Later on, let's see about replacing lower spec creeps with higher when possible.
    _public.getCreepSpecification = function(spawn) { return []; }
    // Do something with a creep in this role. Return undefined/true if the
    // creep is being useful. Return 'false' if not and we'll see about reassigning them.
    _public.run = function(creep, state, jobs) { }

    return _public;
};