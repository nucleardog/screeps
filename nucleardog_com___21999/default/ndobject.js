/*
 * Module code goes here. Use 'module.exports' to export things:
 * module.exports.thing = 'a thing';
 *
 * You can import it from another modules like this:
 * var mod = require('ndobject');
 * mod.thing == 'a thing'; // true
 */

module.exports = function() {
    var _public = new Object(), _parent = new Object(), _private = new Object();

    _public.extend = function(parentPublic) {
        if (parentPublic === undefined) parentPublic = _public;
        var newPublic = new Object();
        for (var k in parentPublic)
            newPublic[k] = parentPublic[k];
        newPublic['extend'] = function(usePublic) {
            if (usePublic === undefined)
                usePublic = newPublic;
            return parentPublic.extend(usePublic);
        };
        return {
            _private: new Object(),
            _public: newPublic,
            _parent: parentPublic
        };
    };

    return _public;
};