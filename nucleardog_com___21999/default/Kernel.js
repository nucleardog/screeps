var ndobject = require('ndobject');
var settings = require('Settings');
var Job = require('Job');

module.exports = function() {
    var {_private, _public, _parent} = new ndobject().extend();
    
    _private.jobs = [];
    _private.managers = {};
    _private.rooms = {};

    _public.initialize = function(managers) {
        for (var i=0; i<managers.length; i++)
            _public.addManager(managers[i]);
        _private.jobs = Job.load();
        _private.sortJobs();
    }

    _public.addJob = function(job) {
        _private.jobs.push(job);
        _private.sortJobs();
    }

    _private.sortJobs = function() {
        _private.jobs.sort(function(a, b) {
            if (a.getPriority() == b.getPriority())
                return 0;
            return a.getPriority() < b.getPriority() ? -1 : 1;
        });
    };
    
    _public.addManager = function(manager) {
        // Get the name of the role this manager handles
        var role = (new manager).getRole();
        // Add the manager to our list of managers
        _private.managers[role] = manager;

        // Initialize an instance of this manager for all existing rooms
        for (var name in _private.rooms)
        {
            _private.rooms[name].managers[role] = new _private.managers[role]();
            _private.rooms[name].managers[role].initialize(_private.rooms[name]);
        }
    }
    
    _public.tick = function(ticks) {
        // Currently there's no grand strategy going on. Run our automation
        // on each room we're present in.
        for (var k in Game.rooms)
        {
            var room = Game.rooms[k];
            _private.tickRoom(room);
        }
    }

    _private.initializeRoom = function(room) {
        var managers = {};
        for (var k in _private.managers)
        {
            managers[k] = new _private.managers[k]();
            managers[k].initialize(room);
        }
        _private.rooms[room.name] = { managers: managers };
    }

    _private.runCreeps = function(managers, creeps) {
        for (var i=0; i<creeps.length; i++)
        {
            var creep = creeps[i];
            var role = creep.memory.role;
            var result = false;
            if (role !== undefined && managers[role] !== undefined)
            {
                var roleState = creep.memory[role+'_state'] === undefined ?
                    managers[role].getDefaultState() :
                    creep.memory[role+'_state'];
                result = managers[role].run(creep, roleState, _private.jobs);
                if (result === false)
                    creep.say("idle");
                creep.memory[role+'_state'] = roleState;
            }

            if (result === false)
                creep.memory.role = undefined;
       }
    }

    _private.findIdleCreeps = function(creeps) {
        var idle = [];
        for (var i=0; i<creeps.length; i++)
        {
            if (creeps[i].memory['role'] === undefined)
                idle.push(creeps[i]);
        }
        return idle;
    }

    _private.calculateAllocations = function(managers, creeps) {
        var allocations = {};
        // Ask the managers what targets they want
        for (var k in managers)
            allocations[k] = { target: managers[k].getTarget(), current: 0 };
        // Count how many creeps are currently assigned to those roles
        for (var i=0; i<creeps.length; i++)
        {
            if (creeps[i].memory['role'] !== undefined)
                allocations[creeps[i].memory.role].current += 1;
        }

        // Calculate a percentage for each allocation bucket
        for (var k in allocations)
        {
            var pcnt = [];
            for (var i=0; i<settings.MANAGER_TARGET_BUCKETS; i++)
            {
                pcnt.push(allocations[k].current / allocations[k].target[i]);
            }
            allocations[k].percent = pcnt;
        }
        return allocations;
    }

    _private.reassignIdleCreeps = function(idle, allocations, managers) {
        // Reassign as appropriate
        // Find all possible applications for the creep
        // Go back through, assign the lowest value in each application for min. - but start from len=1 and work up
        // Continue to do so for meeting mid
        // Then do so for max
        // destroy() any leftovers
        var assignments = {};
        var creepAssignments = {};
        var maxlen = 0;
        for (var i=0; i<idle.length; i++)
        {
            var creep = idle[i];
            maxlen = Math.max(maxlen, creep.body.length);
            var roles = [];
            
            for (var k in managers)
            {
                var specs = managers[k].getCreepSpecification();
                
                for (var j=0; j<specs.length; j++)
                {
                    if (_private.isCreepCompatible(creep, specs[j]))
                    {
                        roles.push(k);
                        break;
                    }
                }
            }
            
            for (var j=0; j<roles.length; j++)
            {
                if (assignments[roles[j]] == undefined)
                    assignments[roles[j]] = [];
                assignments[roles[j]].push(creep);
            }
            
            creepAssignments[creep.name] = roles;
        }
        
        // Work through each percentage bucket - min, mid,m ax
        for (var bucket = 0; bucket < 3; bucket++)
        {
            // Start with the least specialist creep and work upwards
            for (var len = 1; len <= maxlen; len++)
            {
                // Check each role with available creeps
                for (var r in assignments)
                {
                    // Examine each creep
                    for (var i=0; i<assignments[r].length; i++)
                    {
                        // Confirm they're in our current specialization level
                        // Confirm they haven't been assigned elsewhere
                        // Confirm our current role hasn't been assigned more than the allocation for this bucket already
                        if (
                            assignments[r][i].body.length == len &&
                            creep.memory.role == undefined &&
                            allocations[r].current < allocations[r].target[bucket]
                            )
                        {
                            // Go through every role and find the one that needs it most
                            var lowest = 1;
                            for (var j=0; j<creepAssignments[creep.name].length; j++)
                                if (allocations[creepAssignments[creep.name][j]].percent[bucket] < lowest)
                                    lowest = allocations[creepAssignments[creep.name][j]].percent[bucket];
                            
                            // Check to make sure it's us
                            if (lowest == allocations[r].percent[bucket])
                            {
                                creep.memory.role = r;
                                allocations[r].current++;
                                allocations[r].percent = [
                                    allocations[r].current / allocations[r].target[0],
                                    allocations[r].current / allocations[r].target[1],
                                    allocations[r].current / allocations[r].target[2]
                                ];
                                break;
                            }
                        }
                        
                    }
                    
                }
            }
        }
    }

    _private.spawnCreeps = function(room, allocations) {
        // Find the lowest percent that is possible with each spawn's capacity
        var spawns = room.find(FIND_MY_SPAWNS);
        spawns.sort(function(a, b) {
            if (a.energyCapacity == b.energyCapacity) return 0;
            return a.energyCapacity < b.energyCapacity ? -1 : 1;
        })
        for (var bucket=0; bucket<3; bucket++)
        {
            for (var i=0; i<spawns.length; i++)
            {
                var maxCost = spawns[i].energyCapacity;
                var role = null;
                var parts = null;
                var partsCost = 0;
                var rolePcnt = 1;
                
                for (var k in _private.rooms[room.name].managers)
                {
                    if (allocations[k].percent[bucket] >= rolePcnt) continue;
                    var partsLists = _private.rooms[room.name].managers[k].getCreepSpecification();
                    for (var l=0; l<partsLists.length; l++)
                    {
                        var cost = _private.calculateBuildCost(partsLists[l]);
                        if (cost <= maxCost)
                        {
                            role = k;
                            parts = partsLists[l];
                            partsCost = cost;
                            rolePcnt = allocations[k].percent[bucket];
                            break;
                        }
                    }
                }

                if (role != null && rolePcnt < 1)
                {
                    console.log('spawning ' + role + ' at ' + rolePcnt + ' in bucket ' + bucket + ', avail ' + spawns[i].energyCapacity);
                    var name = room.name + '_' + role + '_' + Memory.globalState.ticks + '_';
                    for (var n=0; Game.creeps[name + n] != undefined; n++) { }
                    name += n;
                    var creep = spawns[i].spawnCreep(parts, name, { memory: { role: role } })
                    if (creep == OK)
                    {
                        allocations[role].current++;
                        allocations[role].percent = [
                            allocations[role].current / allocations[role].target[0],
                            allocations[role].current / allocations[role].target[1],
                            allocations[role].current / allocations[role].target[2]
                        ];
                    }
                }
            }
        }
    }
    
    _private.tickRoom = function(room) {

        // If the room hasn't been initialized, initialize it
        if (_private.rooms[room.name] === undefined) 
            _private.initializeRoom(room);

        var roomState = _private.rooms[room.name];
        var creeps = room.find(FIND_MY_CREEPS);

        _private.runCreeps(roomState.managers, creeps);
        var idleCreeps = _private.findIdleCreeps(creeps);
        var allocations = _private.calculateAllocations(roomState.managers, creeps);
        _private.reassignIdleCreeps(idleCreeps, allocations, roomState.managers);
        _private.spawnCreeps(room, allocations);        

        room.memory.debug = {
            allocations: allocations
        };
    }
    
    // Return true if ALL of a are present in b.
    _private.isCreepCompatible = function(creep, b) {
        for (var i=0; i<creep.body.length; i++) {
            var found = false;
            for (var j=0; j<b.length; j++) {
                if (b[j] == creep.body[i].type)
                {
                    found = true;
                    break;
                }
            }
            if (!found) return false;
        }
        return true;
    }
    
    // Return how much it costs to build a creep
    _private.calculateBuildCost = function(parts) {
        var sum = 0;
        var lookup = {
            move: 50,
            work: 100,
            carry: 50,
            attack: 80,
            ranged_attack: 150,
            heal: 250,
            claim: 600,
            tough: 10
        };
        for (var i=0; i<parts.length; i++)
        {
            sum += lookup[parts[i]];
        }
        return sum;
    }

    return _public;
};