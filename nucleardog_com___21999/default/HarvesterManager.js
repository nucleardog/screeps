var manager = require('Manager');

module.exports = function() {
    var {_private, _public, _parent} = new manager().extend();
    
    _private.settings = {
        min: function(base) { return Math.max(base * 0.5, 1); },
        max: function(base) { return base * 2; }
    }
    
    _public.getRole = function() {
        return 'harvester';
    }
    
    _public.getDefaultState = function() {
        return { task: 'collect' };   
    }
    
    _private.getEnergyStorageStructures = function() {
        var room = _parent.getRoom();
        var structures = room.find(FIND_MY_STRUCTURES);
        var filtered = [];
        for (var i=0; i<structures.length; i++) {
            var structure = structures[i];
            if (structure['energyCapacity'] !== undefined)
              filtered.push(structure);
        }
        return filtered;
    }
    
    _public.getTarget = function() {
        var structures = _private.getEnergyStorageStructures();
        var filtered = [];
        for (var i=0; i<structures.length; i++)
            if (structures[i].energy < structures[i].energyCapacity)
                filtered.push(structures[i]);
        return [_private.settings.min(filtered.length), filtered.length, _private.settings.max(filtered.length)];
        
    }
    _public.getCreepSpecification = function() {
        return [
            [CARRY, CARRY, WORK, MOVE, MOVE, MOVE],
            [CARRY, WORK, MOVE, MOVE],
            [CARRY, WORK, MOVE]
        ];
    }
    _public.run = function(creep, state, jobs) {
        switch (state.task)
        {
            case 'collect':
                if (_.sum(creep.carry) >= creep.carryCapacity)
                {
                    creep.say("📦 deposit");
                    state.task = 'deposit';
                }
                else
                {
                    var sources = creep.room.find(FIND_SOURCES), sourceId;
                    for (sourceId = 0; sourceId < sources.length && sources[sourceId].energy == 0; sourceId++) {}
                    if (sourceId == sources.length)
                        return false;
                    var result = creep.harvest(sources[sourceId]);
                    if (result == OK)
                    {
                        break;
                    }
                    else if (result == ERR_NOT_IN_RANGE)
                        creep.moveTo(sources[sourceId]);
                    else if (result == ERR_BUSY)
                        break;
                    else
                        return false;
                }
                break;
            case 'deposit':
                if (creep.carry.energy == 0)
                {
                    creep.say("⛏ collect");
                    state.task = 'collect';
                }
                else
                {
                    var structures = _private.getEnergyStorageStructures();
                    var distances = [];
                    for (var i=0; i<structures.length; i++) {
                        distances.push([creep.pos.findPathTo(structures[i]), structures[i]]);
                    }
                    distances.sort(function(a, b) {
                       if (a[0].length == b[0].length) return 0;
                       return a[0].length < b[0].length ? -1 : 1;
                    });
                    
                    var structureId;
                    for (structureId = 0; structureId < distances.length && distances[structureId][1].energy == distances[structureId][1].energyCapacity; structureId++) { }
                    if (structureId == distances.length)
                        structureId = 0;
                        
                    var result = creep.transfer(distances[structureId][1], RESOURCE_ENERGY);
                    if (result == OK)
                       break;
                    else if (result == ERR_NOT_IN_RANGE)
                    {
                        creep.move(distances[structureId][0][0].direction);
                    }
                    else
                        return false;
                }
                break;
        }
    }

    return _public;
};