var manager = require('Manager');

module.exports = function() {
    var {_private, _public, _parent} = new manager().extend();
    
    _private.settings = {
    }
    
    _public.getRole = function() {
        return 'upgrader';
    }
    
    _public.getDefaultState = function() {
        return { task: 'collect' };
    }
    
    _public.getTarget = function() {
        return [1, 2, 4];
        
    }
    _public.getCreepSpecification = function() {
        return [
            [CARRY, CARRY, WORK, MOVE, MOVE, MOVE],
            [CARRY, WORK, MOVE, MOVE],
            [CARRY, WORK, MOVE]
        ];
    }
    _public.run = function(creep, state, jobs) {
        switch (state.task)
        {
            case 'collect':
                if (_.sum(creep.carry) >= creep.carryCapacity)
                {
                    creep.say("🛠 upgrade");
                    state.task = 'upgrade';
                }
                else
                {
                    var sources = creep.room.find(FIND_SOURCES), sourceId;
                    for (sourceId = 0; sourceId < sources.length && sources[sourceId].energy == 0; sourceId++) {}
                    if (sourceId == sources.length)
                        return false;
                    var result = creep.harvest(sources[sourceId]);
                    if (result == OK)
                    {
                        break;
                    }
                    else if (result == ERR_NOT_IN_RANGE)
                        creep.moveTo(sources[sourceId]);
                    else if (result == ERR_BUSY)
                        break;
                    else
                        return false;
                }
                break;
            case 'upgrade':
                if (creep.carry.energy == 0)
                {
                    creep.say("⛏ collect");
                    state.task = 'collect';
                }
                else
                {
                    var result = creep.upgradeController(creep.room.controller);
                    
                    if (result == OK)
                        break;
                    else if (result == ERR_NOT_IN_RANGE)
                        creep.moveTo(creep.room.controller);
                    else
                        return false;
                }
                break;
        }
    }

    return _public;
};