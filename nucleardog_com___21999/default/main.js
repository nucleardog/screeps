var Kernel = require('Kernel');
var HarvesterManager = require('HarvesterManager');
var UpgraderManager = require('UpgraderManager');

var my_kernel = null;

module.exports.loop = function() {

	if (Memory['globalState'] === undefined)
	{
		Memory.globalState = {ticks:0};
	}

    if (my_kernel === null)
    {
        my_kernel = new Kernel();
        my_kernel.initialize([
            HarvesterManager,
            UpgraderManager
        ]);
    }
    
    Memory.globalState.ticks++;
    my_kernel.tick(Memory.globalState.ticks);

}