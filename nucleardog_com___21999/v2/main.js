var Kernel = require('Kernel');

var my_kernel = null;

module.export.loop = function() {
	if (my_kernel == null)
	{
		my_kernel = new Kernel({
			reset: false,
			roles:[

			],
			goals:[

			]
		});
	}

	my_kernel.tick();
};