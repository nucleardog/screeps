module.exports = function() {
	var _public = new Object(), _parent = new Object(), _private = new Object();

	_public.extend = function(parentPublic) {
		if (parentPublic === undefined) parentPublic = _public;
		var newPublic = new Object();
		for (var k in parentPublic)
			newPublic[k] = parentPublic[k];
		newPublic['extend'] = function(usePublic) {
			if (usePublic === undefined)
				usePublic = newPublic;
			return parentPublic.extend(usePublic);
		};
		return {
			_private: new Object(),
			_public: newPublic,
			_parent: parentPublic
		};
	};

	return _public;
};