var ndobject = require('ndobject');

module.exports = function() {
	var {_private, _public, _parent} = new ndobject().extend();

	_private.kernelState = null;
	_private.components = {
		goals: {},
		roles: {},
		job: require('Job'),
		creepManager: null
	};

	_public.initialize = function(setup) {

		// If we're not forcing a reset, attempt to load existing kernel state
		if (setup['reset'] === true)
		{
			_private.readKernelState();
		}
		// If no kernel state exists (due to reset or first run), initialize
		// a new state object and save it immediately.
		if (_private.kernelState === null)
		{
			_private.initializeKernelState();
			_private.writeKernelState();
		}

		if (setup.goals)
		{
			for (var i=0; i<setup.goals.length; i++)
			{
				var goal = new setup.goals[i]();
				goal.initialize(_public);
				_private.components.goals[goal.getName()] = goal;
			}
		}

		if (setup.roles)
		{
			for (var i=0; i<setup.roles.length; i++)
			{
				var role = new setup.roles[i]();
				role.initialize(_public);
				_private.components.roles[role.getName()] = role;
			}
		}

		_private.components.creepManager = new require('CreepManager')();
		_private.components.creepManager.initialize(_public);
	};

	_private.initializeKernelState = function() {
		_private.kernelState = {
			ticks: 0,
			nextJobId: 0,
			goals: {},
			jobs: []
		};
	};

	_private.readKernelState = function() {
		_private.kernelState = Memory['kernelState'] === undefined ? null : Memory['kernelState'];
	};

	_private.writeKernelState = function() {
		Memory['kernelState'] = _private.kernelState;
	};

	_public.getRoles = function() {
		return _private.components.roles;
	};

	_public.getGoals = function() {
		return _private.components.goals;
	};

	_public.tick = function() {
		_private.kernelState.ticks++;
		_private.kernelState.nextJobId = 0;

		_private.components.creepManager.refresh();

		// Loop through each room
		for (var k in Game.rooms)
		{
			var room = Game.rooms[k];

			// Go through each goal and see if it wants to generate any
			// new jobs. Jobs are added automatically to the kernel's job
			// list.
			_private.tickProcessGoals(room);

			// Get a list of the kernel's unassigned jobs
			var jobs = _public.getJobs();
			var unassigned = [];
			for (var j=0; j<jobs.length; j++)
				if (!jobs[j].isAssigned()
					unassigned.push(jobs[j]);

			if (unassigned.length)
			{
				_private.tickAssignJobs(unassigned);
			}


		}

		// Run through list of jobs that aren't assigned. Figure out how many
		// screeps of each role we need.

		// Ask a ScreepManager to reassign/spawn/etc screeps as necessary.
		// Grab any idle screeps and find them jobs.

		// Run the roles/creeps/jobs as necessary.
	}

	_private.tickAssignJobs = function(jobs) {
		for (var i=0; i<jobs.length; i++)
		{
			var creep = _private.components.creepManager.getCreepForJob(jobs[i]);
			if (creep !== null)
			{
				jobs[i].setCreep(creep);
				_private.components.creepManager.assignCreep(creep);
			}
		}
	};

	_private.tickProcessGoals = function(room) {
		// Go through goals, run where necessary. Merge generated jobs
		// into job list.
		var goals = [];
		for (var k in _private.components.goals)
			goals.push(_private.components.goals[k]);
		goals.sort(function(a, b) {
			var a_p = a.getPriority(), b_p = b.getPriority();
			if (a_p == b_p)
				return 0;
			return a_p < b_p ? -1 : 1;
		});

		for (var i=0; i<goals.length; i++)
		{
			if ((_private.kernelState.ticks % goals[i].getRunInterval()) == 0)
			{
				var name = goals[i].getName();
				if (_private.kernelState.goals[name] === undefined)
					_private.kernelState.goals[name] = {};
				if (_private.kernelState.goals[name][room.name] === undefined)
					_private.kernelState.goals[name][room.name] = goals[i].getDefaultGoalState();
				var roomState = _private.kernelState.goals[name][room.name];
				var jobs = goals[i].tick(
					_private.kernelState.ticks,
					room,
					roomState
					);
				_private.kernelState.goals[name][room.name] = roomState;

				for (var j=0; j<jobs.length; j++)
				{
					_public.commitJob(jobs[j]);
				}
			}
		}
	};

	_public.newJob = function() {
		return new _private.components.Job('j-' + _private.kernelState.ticks + '.' + (_private.kernelState.nextJobId++));
	};

	_public.commitJob = function(job) {
		_private.kernelState.jobs.push(job.serialize());
	};

	_public.completeJob = function(job) {
		for (var i=0; i<_private.kernelState.jobs.length; i++)
		{
			if (_private.kernelState.jobs[i].id == job.getId())
			{
				_private.kernelState.jobs.splice(i, 1);
				return;
			}
		}
	};

	_public.getJob = function(id) {
		for (var j=0; j<_private.kernelState.jobs.length; j++)
		{
			if (_private.kernelState.jobs[j].id == id)
			{
				var job = new _private.components.Job();
				job.unserialize(_private.kernelState.jobs[j]);
				return job;
			}
		}
		return null;
	};

	_public.getJobs = function() {
		var jobs = [];
		for (var j=0; j<_private.kernelState.jobs.length; j++)
		{
			var job = new _private.components.Job();
			job.unserialize(_private.kernelState.jobs[j]);
			jobs.push(job);
		}
		return jobs;
	};

	return _public;
};