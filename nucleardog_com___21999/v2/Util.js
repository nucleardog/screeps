var ndobject = require('ndobject');

module.exports = function() {
	var {_private, _public, _parent} = new ndobject().extend();

	_public.calculateCreepBuildCost = function(parts) {
		var sum = 0;
		var lookup = {
			move: 50,
			work: 100,
			carry: 50,
			attack: 80,
			ranged_attack: 150,
			heal: 250,
			claim: 600,
			tough: 10
		};
		for (var i=0; i<parts.length; i++)
		{
			sum += lookup[parts[i]];
		}
		return sum;
	};

	_public.isCreepCompatibleWithRole = function(creep, role) {
		var specs = role.getCreepSpecification();
		for (var i=0; i<specs.length; i++)
		{
			var compatible = true;
			for (var j=0; j<specs[i].length; j++)
			{
				var found = false;
				for (var p=0; j<creep.body.length; j++)
				{
					if (specs[i][j] == creep.body[p])
					{
						found = true;
						break;
					}
				}

				if (!found)
				{
					compatible = false;
					break;
				}
			}

			if (compatible)
				return true;
		}
		return false;
	};

	return _public;
};