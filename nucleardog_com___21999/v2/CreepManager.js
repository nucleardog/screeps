var ndobject = require('ndobject');

module.exports = function() {
	var {_private, _public, _parent} = new ndobject().extend();

	_private.kernel = null;
	_private.util = require('Util');
	_private.state = {
		assignableRoles: null,
		idle: [],
		idleByRole: {}
	};

	/**
	* Initialize the job
	*
	* @param Kernel kernel
	* @return void
	*/
	_public.initialize = function(kernel) {
		_private.kernel = kernel;
		_private.refreshAssignedRoles();
	};

	_private.refreshAssignedRoles = function() {
		// Go through each role and figure out which can
		// satisfy which jobs
		var roles = _private.kernel.getRoles();
		var jobToRole = {};
		for (var k in roles)
		{
			var assignable = _private.components.roles[k].getAssignableJobTypes();
			for (var i=0; i<assignable.length; i++)
			{
				if (jobToRole[assignable[i]] === undefined)
					jobToRole[assignable[i]] = [];
				jobToRole[assignable[i]].push(k);
			}
		}
		_private.state.assignableRoles = jobToRole;
	};

	_public.refresh = function() {
		_private.state.idle = [];
		_private.state.idleByRole = {};

		var creeps = Game.creeps;
		for (var k in creeps) {
			if (creeps[k].memory['job_id'] === undefined)
			{
				_private.state.idle.push(creeps[k]);

				var roles = _private.kernel.getRoles();
				for (var i=0; i<roles.length; i++)
				{
					if (_private.util.isCreepCompatibleWithRole(creeps[k], roles[i]))
					{
						var role = roles[i].getName();
						if (_private.state.idleByRole[role] === undefined)
							_private.state.idleByRole[role] = [];
						_private.state.idleByRole[role].push(creeps[k]);
					}
				}
			}
		}

		for (var role in _private.state.idleByRole)
		{
			_private.state.idleByRole[role].sort(function(a, b) {
				var a_c = _private.util.calculateCreepBuildCost(a.body);
				var b_c = _private.util.calculateCreepBuildCost(b.body);
				if (a_c == b_c)
					return 0;
				return a_c < b_c ? -1 : 1;
			});
		}
	};

	_public.getCreepForJob = function(job) {

		var possibleRoles = [];
		var roles = _private.kernel.getRoles();
		var jobName = job.getName();
		for (var r in roles)
		{
			var supported = roles[r].getAssignableJobTypes();
			for (var i=0; i<supported.length; i++)
			{
				if (supported[i] == jobName)
				{
					possibleRoles.push(r);
					break;
				}
			}
		}

		var possible = [];
		for (i=0; i<possibleRoles.length; i++)
		{
			if (_private.state.idleByRole[possibleRoles[i]] !== undefined)
				possible = possible.concat(_private.state.idleByRole[possibleRoles[i]]);
		}

		var target = job.getPosition();
		var toSort = [];
		for (var i=0; i<possible.length; i++)
			toSort.push([possible[i].pos.findPathTo(target).length, possible[i]]);
		toSort.sort(function(a, b) {
			if (a[0] == b[0]) return 0;
			return a[0] < b[0] ? -1 : 1;
		});

		if (toSort.length == 0)
			return null;

		return toSort[0][1];
	};

	_public.assignCreep = function(creep) {
		for (var i=0; i<_private.state.idle.length; i++)
		{
			if (_private.state.idle[i].name == creep.name)
			{
				_private.state.idle.splice(i, 1);
				i--;
			}
		}

		for (var r in _private.state.idleByRole)
		{
			for (var i=0; i<_private.state.idleByRole[r].length; i++)
			{
				if (_private.state.idleByRole[r][i].name == creep.name)
				{
					_private.state.idleByRole[r].splice(i, 1);
					i--;
				}

			}
		}
	};

	_public.getSpawnForJob = function(job) {
		var roles = _private.state.assignableRoles[job.getName()];
		if (

	};

	return _public;
};