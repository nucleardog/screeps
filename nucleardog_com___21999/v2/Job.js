var ndobject = require('ndobject');

module.exports = function(id) {
	var {_private, _public, _parent} = new ndobject().extend();

	_private.kernel = null;
	_private.id = id;
	_private.name = null;
	_private.creep = null;
	_private.position = null;
	_private.state = null;

	/**
	* Initialize the job
	*
	* @param Kernel kernel
	* @return void
	*/
	_public.initialize = function(kernel) {
		_private.kernel = kernel;
	};

	/**
	* Retrieve the kernel we were initialized with
	*
	* @return Kernel
	*/
	_public.getKernel = function() {
		return _private.kernel;
	};

	/**
	* Retrieve the friendly string name of this job.
	*
	* @return string
	*/
	_public.getName = function() {
		return _private.name;
	};

	/**
	* Set the job name
	*
	* @param string name
	* @return void
	*/
	_public.setName = function(name) {
		_private.name = name;
	};

	/**
	* Get an object representing the default state of a
	* new job of this type.
	*
	* @return object
	*/
	_public.getDefaultJobState = function() {
		return {};
	};

	/**
	* Get the location this job requires a screep at.
	*
	* @return RoomPosition
	*/
	_public.getPosition = function() {
		return _private.position;
	};

	/**
	* Set the location this job requires a screep at.
	*
	* @param RoomPosition position
	* @return void
	*/
	_public.setPosition = function(position) {
		_private.position = position;
	};

	/**
	* Fetch the job's data object containing any job-specific
	* information
	*
	* @return object
	*/
	_public.getState = function() {
		if (_private.state == null)
			_private.state = _public.getDefaultJobState();
		return _private.state;
	};

	/**
	* Overwrite the job's data object.
	*
	* @param object data
	* @return void
	*/
	_public.setState = function(state) {
		_private.state = data;
	};

	/**
	* Get this job's unique id
	*
	* @return string
	*/
	_public.getId = function() {
		return _private.id;
	};

	/**
	* Check if this job is assigned
	*
	* @return bool
	*/
	_public.isAssigned = function() {
		return _private.creep !== null;
	};

	/**
	* Get the creep assigned to this job
	*
	* @return Creep
	*/
	_public.getCreep = function() {
		if (_private.creep === null)
			return null;
		return Game.creeps[_private.creep];
	};

	/**
	* Set the creep this job is assigned to
	*
	* @param Creep creep
	* @return void
	*/
	_public.setCreep = function(creep) {
		_private.creep = creep.name;
		creep.memory.job_id = _private.id;
	};

	/**
	* Package up this job for storage
	*
	* @return object
	*/
	_public.serialize = function() {
		return {
			id: _private.id,
			name: _private.name,
			creep: _private.creep,
			position: _private.position,
			state: _private.state
		};
	};

	/**
	* Restore this object from a stored state
	*
	* @param object state
	* @return void
	*/
	_public.unserialize = function(state) {
		_private.id = state.id;
		_private.name = state.name;
		_private.creep = state.creep;
		_private.position = state.position;
		_private.state = state.state;
	};

	return _public;
};