var ndobject = require('ndobject');

module.exports = function() {
	var {_private, _public, _parent} = new ndobject().extend();

	_private.kernel = null;

	/**
	* Initialize the goal
	*
	* @param Kernel kernel
	* @return void
	*/
	_public.initialize = function(kernel) {
		_private.kernel = kernel;
	};

	/**
	* Retrieve the kernel we were initialized with
	*
	* @return Kernel
	*/
	_public.getKernel = function() {
		return _private.kernel;
	};

	/**
	* Retrieve the friendly string name of this goal.
	*
	* @return string
	*/
	_public.getName = function() {
	};

	/**
	* Get an object representing the default state of a
	* new room.
	*
	* @return object
	*/
	_public.getDefaultGoalState = function() {
		return {};
	};

	/**
	* Check the world state and generate any jobs that
	* are necessary.
	*
	* @param integer ticks
	* @param Room room
	* @param object roomState
	* @return Job[]
	*/
	_public.tick = function(ticks, room, roomState) {
	};

	/**
	* Get the priority of this goal. In case of limited
	* resources or other conflicts, this will resolve ties.
	*
	* @return integer
	*/
	_public.getPriority = function() {
		return 0;	// PRIORITY.NORMAL
	};

	/**
	* Get the run inteval of this goal in ticks. The tick()
	* method will only be called every this many ticks.
	*
	* @return integer
	*/
	_public.getRunInterval = function() {
		return 1;
	};

	return _public;
};