module.exports = {
	NEVER:   -100000,
	LOWEST:    -1000,
	LOW:        -100,
	NORMAL:        0,
	HIGH:        100,
	HIGHEST:    1000,
	EMERGENCY:100000
};