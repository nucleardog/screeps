var ndobject = require('ndobject');

module.exports = function() {
	var {_private, _public, _parent} = new ndobject().extend();

	_private.kernel = null;

	/**
	* Initialize the goal
	*
	* @param Kernel kernel
	* @return void
	*/
	_public.initialize = function(kernel) {
		_private.kernel = kernel;
	};

	/**
	* Retrieve the kernel we were initialized with
	*
	* @return Kernel
	*/
	_public.getKernel = function() {
		return _private.kernel;
	};

	/**
	* Retrieve the friendly string name of this role.
	*
	* @return string
	*/
	_public.getName = function() {
	};

	/**
	* Retrieve the types of jobs this role will take on.
	*
	* @return string[]
	*/
	_public.getAssignableJobTypes = function() {
		return [];
	};

	/**
	* Get an object representing the default state of a
	* new creep born into this role.
	*
	* @return object
	*/
	_public.getDefaultCreepState = function() {
		return {};
	};

	/**
	* Get an array of creep body specifications that this
	* role will use. These should be ordered from most
	* desirable to least desirable.
	*
	* e.g. [[CARRY, CARRY, MOVE, MOVE], [CARRY, MOVE]]
	*
	* @return array
	*/
	_public.getCreepSpecification = function() {
		return [];
	};

	/**
	* Have this creep perform the job we've selected for it.
	*
	* @param Creep creep
	* @param object state
	* @param Job job
	* @return bool true if successful, false if failed (job will be reassigned)
	*/
	_public.run = function(creep, state, job) {
	};

	return _public;
};